# Development

## Architecture
Bughunting server currently uses SCP / SSH calls, with root privileges, to access files on clients, or to make other changes. We currently aim to change that.

### Tasks
* Instead of copying sources from client add an option into task to send diffs to server. !34
* Add build_commands option to define commands that need to be run to build the task.
  * Display the commands in the web UI.
* After copying files, execute the rest of the jail under the user submitted the task.
* Send a patch of sources for check instead of copying the files over SSH
  * send the whole file as a patch if copy_task_on_server option is false.
  * create a diff if copy_task_on_server option is true.
* Apply the diff or create a new file from the diff.
* diff validation: check for valid files.
