#!/usr/bin/bash
set -xe
bash -n "$0"

NAME="bughunting"
VERSION=`rpmspec -P *.spec | ruby -ne 'if $_ =~ /^\s*Version:/ ; puts $_.split.last ; exit ; end'`

VERSIONED_NAME="$NAME-$VERSION"

# Create archive from current state of the files in git as opposed to checkout out just the HEAD with `git archive`
tar -cJf $VERSIONED_NAME.tar.xz `git ls-files` --xform="s|^|$VERSIONED_NAME/|S"

rpmbuild --define "_sourcedir $PWD/rpm-build-dir" --define "_srcrpmdir $PWD/rpm-build-dir" --define "_rpmdir $PWD/rpm-build-dir" --define "_specdir $PWD/rpm-build-dir" -tb $VERSIONED_NAME.tar.xz 
