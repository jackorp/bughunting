require 'bughunting/common/local_execute'

RSpec.describe LocalExecute do

  class Foo
    include LocalExecute
  end

  describe '#local_execute' do
    let(:foo) { Foo.new }

    context 'with array as input' do
      context 'when return code is 0' do
        it 'returns true' do
          valid_command = ['ls', '-al']
          expect(foo.local_execute(valid_command)).to be true
        end
      end

      context 'when return code is not 0' do
        it 'returns false' do
          invalid_command = ['false', 'foo']
          expect(foo.local_execute(invalid_command)).to be false
        end
      end
    end

    context 'with string as input' do
      context 'when return code is 0' do
        it 'returns true' do
          valid_command = 'ls -al'
          expect(foo.local_execute(valid_command)).to be true
        end
      end

      context 'when return code is not 0' do
        it 'returns false' do
          invalid_command = 'false foo'
          expect(foo.local_execute(invalid_command)).to be false
        end
      end
    end

    context 'when debug is true' do
      let(:command) { 'echo hello' }

      it 'prints original command' do
        expect { foo.local_execute(command, debug: true) }.to output(/\+ echo hello/).to_stderr
      end

      it 'prints output of the command' do
        expect { foo.local_execute(command, debug: true) }.to output(/out: hello/).to_stderr
      end

      it 'prints error output of the command' do
        command << ' 1>&2'

        expect { foo.local_execute(command, debug: true) }.to output(/err: hello/).to_stderr
      end

      it 'prints status of the process' do
        expect { foo.local_execute(command, debug: true) }.to output(/result: pid \d* exit \d/).to_stderr
      end
    end

    context 'when debug is false' do
      let(:command) { 'echo hello' }

      it 'does not print anything' do
        expect{ foo.local_execute(command) }.not_to output.to_stderr
      end
    end

    context 'with defined :out argument' do
      let(:command) { 'echo hello' }
      let(:out) { '' }

      it 'appends output of the command' do
        foo.local_execute(command, out: out)
        expect(out).to eq "hello\n"
      end
    end

    context 'with defined :err argument' do
      let(:command) { 'echo hello 1>&2' }
      let(:err) { '' }

      it 'appends error output of the command' do
        foo.local_execute(command, err: err)
        expect(err).to eq "hello\n"
      end
    end
  end
end
