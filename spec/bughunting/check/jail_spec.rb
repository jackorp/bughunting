require 'bughunting/check/jail'
require 'bughunting/check/task'
require 'bughunting/check/carrier'

RSpec.describe Jail do
  describe '#create' do
    let(:carrier) { double("RemoteCarrier") }
    let(:task_path) { File.expand_path('./spec/fixtures/warm_up/') }
    let(:task) { Task.new(task_path) }
    let(:jail) { Jail.new(nil) }

    subject do
      jail.create(task, carrier)
      jail
    end

    before do
      allow(carrier).to receive(:get) { true }
    end

    after do
      FileUtils.remove_dir subject.path
    end

    it 'creates temporary directory' do
      expect(File).to exist(subject.path)
    end

    it 'copies player files' do
      expect(carrier).to have_received(:get).with(subject.exec_dir, task.config['check_files'])
    end

    it 'copies check files' do
      check_file = File.join(subject.exec_dir, task.config['check_script'])

      expect(File).to exist(check_file)
    end

    context 'when copy_task_on_server is true' do
      before do
        task.config['copy_task_on_server'] = true
      end

      it 'copies source files from task dir' do
        source_files_before_copy = Dir.children task.source_dir
        source_files_copied_in_jail = Dir.children subject.exec_dir
        # We don't care about the check file
        source_files_copied_in_jail.delete 'check.sh'

        expect(source_files_before_copy).to match_array(source_files_copied_in_jail)
      end
    end

    context 'when copy_task_on_server is false' do
      before do
        task.config['copy_task_on_server'] = false
      end

      it 'copies files from client only' do
        sources_in_jail = Dir.children(subject.exec_dir) - ['check.sh']
        # We don't care about the check file

        expect(sources_in_jail).to be_empty
      end
    end
  end
end
