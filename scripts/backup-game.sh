#!/bin/sh

tar --remove-files -cvJf /var/lib/bughunting/archive/bh-game-`date +%F_%R`.tar.xz /var/lib/bughunting/results.db /var/lib/bughunting/running /var/lib/bughunting/archive/192.168.99.*
