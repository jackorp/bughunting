#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/check/carrier"
require "bughunting/check/task"
require "bughunting/server/config"
require "bughunting/server/server"
require "bughunting/server/service"
require "bughunting/server/users"

class ServerController
  def initialize(config_filename, local)
    @config_filename = config_filename
    @local = local
  end

  def run
    config = ServerConfig.load_file @config_filename

    clients_sources = config["clients.sources_dir"]
    carrier_factory = CarrierFactory.new clients_sources

    if @local
      user = config["users"][0]
      service = DevelServerService.new config, user, carrier_factory
    else
      tasks = Task.load_dir config["paths.tasks"]
      users = Users.new config["users"]
      service = CompetitionServerService.new config, users, tasks, carrier_factory
    end

    server = Server.new config, service, @local
    server.serve
  end
end
