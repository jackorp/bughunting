#!/usr/bin/ruby

threads = [
  Thread.new { `huntserver` },
  Thread.new { `/usr/share/bughunting/web/web.rb` }
]

threads.each { |thread| thread.join }
